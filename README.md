# Search API Fast

Search API Fast provides you with a drush command to (re)index your
search_api indexes. In the event of a large index, Search API Fast
dramatically increases
indexing speed by using a multitude of simultaneous workers, making
use of all
you server's cores.

Note this module is dependent on drush, search_api and a unix/linux environment.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/search_api_fast).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/search_api_fast).


## Requirements

- Unix / Linux based operating system
- [Search API](https://www.drupal.org/project/search_api)
- [Search API Solr](https://www.drupal.org/project/search_api_solr)
- [Drush](https://www.drush.org)


## Installation

Install as you would normally install a contributed Drupal module. For further
information,see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

Also install and configure Search API and Search API Solr.


## Configuration

Optionally, you can change the default settings:

- _index_workers_: amount of simultaneous workers for indexing queues.
  This should not be more than the available
  cores.
- _worker_batch_size_: Batch size (items to index). Each worker handles batches
  from it's own queue.
- _max_batches_worker_respawn_: Amount of batches to be handled by each worker
  before it's respawned.
  If changing this, you should take memory consumption into account (more
  batches mean higher amount of non-collected memory pages)
- _drush_: path to drush script

If you want to change any of these add it to your settings.php file. In UI mode
you can go to admin/config/search/search-api-fast

````
$config['search_api_fast.performance']['index_workers'] = 8;
$config['search_api_fast.performance']['worker_batch_size'] = 100;
$config['search_api_fast.performance']['max_batches_worker_respawn'] = 4;
$config['search_api_fast.performance']['drush'] = '/opt/mycooldrush/drush';
````

## Usage

To start indexing:
````drush search-api-fast:index [index-name]````
You could use 'top' or 'ps -ef | grep drush' to see all workers spawn.

To reindex:
````drush search-api-fast:index [index-name] reindex````

To clear the index and then reindex:
````drush search-api-fast:index [index-name] clear````

To index everything with use of multiple workers:
````drush search-api-fast:index-queue [index-name]````

Workers are started by executing new drush commands from within this module.
After a worker has handled a configurable amount of items, it respawns itself
in order to make sure
memory householding doesn't present a problem. This way we won't have to rely
on the php garbage collector.


## Maintainers

Current maintainers:
- [Fabian de Rijk](https://www.drupal.org/u/fabianderijk)
- [Johan den Hollander](@https://www.drupal.org/u/johan-den-hollander)
- [Reinier Vegter](https://www.drupal.org/u/reinier-v)
- [Tjerk de Baat](https://www.drupal.org/u/tjerk-de-baat)
- [Tom van Vliet](https://www.drupal.org/u/tomvv)
