<?php

namespace Drupal\search_api_fast\Constants;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Provides configuration constants for the Search API Fast module.
 */
class SearchApiFastConfig {

  /**
   * The number of workers for indexing queues.
   *
   * @var int
   */
  public static $indexWorkers;

  /**
   * The number of batches to be handled by each worker before it's respawned.
   *
   * @var int
   */
  public static $maxBatchesWorkerRespawn;

  /**
   * The number of batch sizes (items to index).
   *
   * @var int
   */
  public static $workerBatchSize;

  /**
   * The Drush binary.
   *
   * @var string
   */
  public static $drush;

  /**
   * Initializes the configuration constants.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public static function init(ConfigFactoryInterface $config_factory) {
    $config = $config_factory->get('search_api_fast.performance');
    self::$indexWorkers = $config->get('index_workers');
    self::$maxBatchesWorkerRespawn = $config->get('max_batches_worker_respawn');
    self::$workerBatchSize = $config->get('worker_batch_size');
    self::$drush = $config->get('drush');
  }

}
