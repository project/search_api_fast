<?php

namespace Drupal\search_api_fast\Commands;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Database;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\search_api\Entity\Index;
use Drupal\search_api\SearchApiException;
use Drupal\search_api\Utility\CommandHelper;
use Drupal\search_api_fast\SearchApiFastQueue;
use Drush\Commands\DrushCommands;
use Drush\Drush;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Drush commands for search_api_fast.
 */
class SearchApiFastCommands extends DrushCommands {

  /**
   * The command helper.
   *
   * @var \Drupal\search_api\Utility\CommandHelper
   */
  protected $commandHelper;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Search API fast index workers.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $searchApiFastIndexWorkers;

  /**
   * Maximum amount of batches.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $maxBatchesWorkerRespawn;

  /**
   * Worker batch size.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $workerBatchSize;

  /**
   * Drush script.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $drush;

  /**
   * The Drupal logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The Drupal config.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Constructs a SearchApiFastCommands object.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher
   *   The event dispatcher.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager, ModuleHandlerInterface $moduleHandler, EventDispatcherInterface $eventDispatcher, LoggerChannelFactoryInterface $logger_factory, ConfigFactoryInterface $config_factory) {
    $this->commandHelper = new CommandHelper($entityTypeManager, $moduleHandler, $eventDispatcher, 'dt');
    $this->entityTypeManager = $entityTypeManager;
    $this->loggerFactory = $logger_factory->get('search_api_fast');
    $this->configFactory = $config_factory->get('search_api_fast.performance');

    $this->searchApiFastIndexWorkers = $this->configFactory->get('index_workers');
    $this->maxBatchesWorkerRespawn = $this->configFactory->get('max_batches_worker_respawn');
    $this->workerBatchSize = $this->configFactory->get('worker_batch_size');
    $this->drush = $this->configFactory->get('drush');
  }

  /**
   * {@inheritdoc}
   */
  public function setLogger(LoggerInterface $logger): void {
    parent::setLogger($logger);
    $this->commandHelper->setLogger($logger);
  }

  /**
   * Indexes everything with use of multiple workers.
   *
   * @param string $index_name
   *   Index ID to run on.
   * @param string $clear
   *   Provide "clear" to clear the index first or "reindex" to reindex.
   *
   * @command search-api-fast:index
   *
   * @usage drush sapi-fast [index] clear
   *   Index [index], and clear it first (no reindex)
   * @usage drush sapi-fast [index] reindex
   *   Index [index], and mark for reindex first
   * @usage drush sapi-fast [index]
   *   Index without clearing, just what is left un-indexed.
   *
   * @aliases sapi-fast,search-api-index-fast,search:api-index-fast
   */
  public function apiIndexFast($index_name = '', $clear = '') {
    $indexes = $this->commandHelper->loadIndexes([$index_name]);

    if ($index_name && $indexes) {

      $index = reset($indexes);

      // Checks for other processes that may interfere.
      if ($this->isAlreadyRunning($index_name)) {
        $this->output()->writeln('Some other process already running commands... Exit.');
        exit;
      }

      // Clears index.
      if ($clear == 'clear') {
        $index->clear();
      }
      elseif ($clear == 'reindex') {
        $index->reindex();
      }

      // Gets items to index.
      $tracker = $index->getTrackerInstance();
      $items = $tracker->getRemainingItems();
      if ($items) {

        // Checks again for already running processes that would be
        // spawned.
        if ($this->isAlreadyRunning($index_name)) {
          $this->output()->writeln('Some other process already running my commands... Exit.');
          exit;
        }

        // Initiates queues. One for each concurrent process.
        $queues = [];
        for ($worker = 0; $worker < $this->searchApiFastIndexWorkers; $worker++) {
          $queues[$worker] = $this->getQueue('search_api_fast_index_fast_' . $index_name . '_' . $worker);

          // Attempts to recreate existing queues.
          $queues[$worker]->createQueue();
        }

        // Adds items to queue.
        $queue_index = 0;
        $item_queues = [];
        foreach ($items as $item) {
          $item_queues[$queue_index][] = $item;

          // Cycles through queues.
          if ($queue_index == (count($queues) - 1)) {
            $queue_index = 0;
          }
          else {
            $queue_index++;
          }
        }

        // Creates queues.
        foreach ($item_queues as $queue_index => $items) {
          $queues[$queue_index]->createItems($items);
        }

        // Spawns new process for each queue.
        foreach ($item_queues as $queue_index => $items) {
          if (!empty($items)) {
            $this->fastIndexQueueInvoke($index_name, $queue_index);
          }
        }
      }
    }
    else {

      // Prints indexes.
      Drush::drush(Drush::aliasManager()->getSelf(), 'sapi-l');
    }
  }

  /**
   * Indexes everything with use of multiple workers: queue worker.
   *
   * @param string $index_name
   *   Index ID.
   * @param string $worker
   *   The worker id, 0 to $this->searchApiFastIndexWorkers.
   *
   * @command search-api-fast:index-queue
   *
   * @aliases sapi-ifq,search-api-index-fast-queue,search:api-index-fast-queue
   */
  public function apiIndexFastQueue($index_name, $worker) {
    if ($worker >= 0 && $worker <= $this->searchApiFastIndexWorkers) {

      // Gets queue for this worker.
      $queue = $this->getQueue('search_api_fast_index_fast_' . $index_name . '_' . $worker);

      // Gets number of items.
      $count = $queue->numberOfItems();
      if ($count) {

        // Creates small batches to index: $items will hold each batch.
        $item_lists = [];
        while (count($item_lists) < $this->maxBatchesWorkerRespawn && $items = $queue->claimItems($this->workerBatchSize)) {
          $item_lists[] = $items;
          $queue->deleteItems(array_keys($items));
        }

        $indexes = $this->commandHelper->loadIndexes([$index_name]);

        // Gets index.
        if ($indexes) {
          $index = reset($indexes);

          // Indexes each batch.
          foreach ($item_lists as $item_list) {
            try {
              $items = $index->loadItemsMultiple($item_list);

              // Indexes the items.
              $index->indexSpecificItems($items);

              // Clears entity cache. If this is not done, it creates memory
              // problems.
              $this->resetEntityCache($index);
            }
            catch (SearchApiException $e) {
              $this->output()->writeln($e);
              return FALSE;
            }
          }

          // Respawns.
          if ($queue->numberOfItems()) {
            $this->respawn();
          }
        }
      }
    }
    return TRUE;
  }

  /**
   * Queues already spawned workers.
   *
   * @param string $index_name
   *   Index name.
   *
   * @return bool
   *   TRUE: yes. FALSE: no queue workers around.
   */
  protected function isAlreadyRunning($index_name) {
    exec("ps -ef | grep 'search-api-index-fast-queue " . escapeshellarg($index_name) . "\b' | grep -v grep", $proclist);
    if (!empty($proclist)) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Loads queue object.
   *
   * Sets, gets and claims multiple items. Saves a large amount of queries.
   *
   * @param string $name
   *   Queue name.
   *
   * @return \Drupal\search_api_fast\SearchApiFastQueue
   *   Queue object.
   */
  protected function getQueue($name) {
    return new SearchApiFastQueue($name, Database::getConnection());
  }

  /**
   * Spawns new drush process to start worker for indexing items.
   *
   * @param string $index_name
   *   Sapi index.
   * @param int $worker
   *   Queue number.
   */
  protected function fastIndexQueueInvoke($index_name, $worker) {
    global $base_url;

    // Gets drush binary from script runtime parameters.
    $drush = escapeshellarg($this->drush);
    if (php_sapi_name() == 'cli') {
      global $argv;
      if (strpos($argv[0], 'drush') !== FALSE) {
        $drush = escapeshellcmd($argv[0]);
      }
    }

    exec('nohup ' . $drush . ' --uri=' . $base_url . ' search-api-index-fast-queue ' . $index_name . ' ' . $worker . ' > /dev/null 2>&1 &');
  }

  /**
   * Resets entity cache.
   *
   * @param Drupal\search_api\Entity\Index $index
   *   The index for which to reset the entity caches.
   */
  protected function resetEntityCache(Index $index) {
    $index_datasources = $index->getDatasourceIds();
    $reset_entity_types = array_map(function ($value) {
      if (strpos($value, 'entity:') !== FALSE) {
        return str_replace('entity:', '', $value);
      }
      return NULL;
    }, $index_datasources);

    $valid_types = array_keys($this->entityTypeManager->getDefinitions());
    foreach ($reset_entity_types as $entity_type) {
      if (!is_null($entity_type)) {
        if (array_search($entity_type, $valid_types)) {
          $this->entityTypeManager->getStorage($entity_type)->resetCache();
        }
      }
    }

    gc_collect_cycles();
  }

  /**
   * Respawns if this is a drush process.
   *
   * Used to address memory issues.
   */
  protected function respawn() {
    if (php_sapi_name() == 'cli') {
      global $argv;

      if (strpos($argv[0], 'drush') !== FALSE) {
        $cmd = escapeshellcmd($argv[0]);
        unset($argv[0]);
        $params = [];
        foreach ($argv as $arg) {
          $params[] = escapeshellarg($arg);
        }
        $cmd .= ' ' . implode(' ', $params);

        $this->loggerFactory->debug(dt('Respawning: :cmd', [':cmd' => $cmd]));
        exec('nohup ' . $cmd . ' > /dev/null 2>&1 &');
        exit();
      }
    }
  }

}
