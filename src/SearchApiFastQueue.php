<?php

namespace Drupal\search_api_fast;

use Drupal\Core\Queue\DatabaseQueue;

/**
 * Provides queue functions.
 */
class SearchApiFastQueue extends DatabaseQueue {

  /**
   * Creates a number of items in one query.
   *
   * @param array $data
   *   Array containing data.
   *   Each item will become a separate queue item.
   *
   * @return bool
   *   True if insert succeeded, false if failed.
   */
  public function createItems(array $data) {

    $query = $this->connection->insert(static::TABLE_NAME)
      ->fields(['name', 'data', 'created']);

    $time = time();
    foreach ($data as $item) {
      $record = [
        'name' => $this->name,
        'data' => serialize($item),
        'created' => $time,
      ];

      $query->values($record);
    }
    return (bool) $query->execute();
  }

  /**
   * Claims queue items for certain amount of time.
   *
   * The normal claimItem() method only claims 1 item. This allows claiming
   * multiple items.
   *
   * @param int $limit
   *   Maximum number of items.
   * @param int $lease_time
   *   Lease time.
   *
   * @return array|bool
   *   Claimed items, of FALSE on failure.
   */
  public function claimItems($limit = 10, $lease_time = 30) {

    // Claims an item by updating its expiration fields. If claim is not
    // successful another thread may have claimed the item in the meantime.
    // Therefor loop until an item is successfully claimed or is
    // reasonably sure there are no unclaimed items left.
    while (TRUE) {
      $items = $this->connection->queryRange('SELECT item_id, data FROM {' . static::TABLE_NAME . '} q WHERE expire = 0 AND name = :name ORDER BY created, item_id ASC', 0, $limit, [':name' => $this->name])->fetchAllAssoc('item_id');
      if ($items) {

        // Tries to update the item. Only one thread can succeed in updating
        // the same row. It cannot rely on REQUEST_TIME because items might be
        // claimed by a single consumer which runs longer than 1 second. If it
        // continues to use REQUEST_TIME instead of the current time(),
        // time from the lease time is stolen and will tend to reset items
        // before the lease should really expire.
        $update = $this->connection->update(static::TABLE_NAME)
          ->fields([
            'expire' => time() + $lease_time,
          ])
          ->condition('item_id', array_keys($items), 'IN')
          ->condition('expire', 0);

        // If there are affected rows, this update succeeded.
        if ($update->execute()) {
          $data = [];
          foreach ($items as $item_id => $item) {
            $data[$item_id] = unserialize($item->data, ['allowed_classes' => FALSE]);
          }
          return $data;
        }
      }
      else {

        // No items currently available to claim.
        return FALSE;
      }
    }
    return FALSE;
  }

  /**
   * Deletes multiple items from queue.
   *
   * @param array $items
   *   Array containing queue-item ID's.
   */
  public function deleteItems(array $items) {
    $this->connection->delete(static::TABLE_NAME)
      ->condition('item_id', $items, 'IN')
      ->execute();
  }

}
