<?php

namespace Drupal\search_api_fast\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class to define settings in the module.
 */
class SearchApiFastSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'search_api_fast_settings';
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return ['search_api_fast.performance'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('search_api_fast.performance');
    $form['index_workers'] = [
      '#type' => 'number',
      '#title' => $this->t('Index Workers'),
      '#default_value' => $config->get('index_workers'),
      '#min' => 0,
      '#max' => 10,
      '#description' => $this->t('Amount of simultaneous workers for indexing queues.'),
    ];
    $form['worker_batch_size'] = [
      '#type' => 'number',
      '#title' => $this->t('Worker batch size'),
      '#default_value' => $config->get('worker_batch_size'),
      '#min' => 0,
      '#max' => 150,
      '#description' => $this->t('Batch size (items to index). Each worker handles batches from it is own queue.'),
    ];
    $form['max_batches_worker_respawn'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum number of batches'),
      '#default_value' => $config->get('max_batches_worker_respawn'),
      '#min' => 0,
      '#max' => 10,
      '#description' => $this->t('Amount of batches to be handled by each worker before it is respawned.'),
    ];
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#button_type' => 'primary',
      '#value' => $this->t('Save settings'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $op = (string) $values['op'];
    $this->messenger()->addStatus($this->t('The configuration options have been saved.'));
    if ($op == $this->t('Save configuration')) {
      $this->config('search_api_fast.performance')
        ->set('index_workers', $values['index_workers'])
        ->set('worker_batch_size', $values['worker_batch_size'])
        ->set('max_batches_worker_respawn', $values['max_batches_worker_respawn'])
        ->save();
      parent::submitForm($form, $form_state);
    }
  }

}
